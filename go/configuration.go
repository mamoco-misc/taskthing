package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"

	"github.com/smallfish/simpleyaml"
)

/*
 *	@TODO:
 *	- Metod för att se att vi har rättigheter att skriva till `workdir`.
 *  - https://stackoverflow.com/questions/38657154/set-a-global-variable-only-once-in-golang
 *  - Set default values!?
 *	- Add configDir to config
 */

func init() {

}

func validateConfiguration() simpleyaml.Yaml {
	fmt.Printf("Validating conf... ")
	return findConfig()
	//config = parseConfig(data)
	// checkRequired(config)
}

func findConfig() simpleyaml.Yaml {
	path := "./config.yml"
	// ~/.config/taskthing.yml
	// ~/.config/taskthing/config.yml
	// ~/.taskthing.yml
	setConfigPath()
	createNote()
	return readPath(path)
}

func getNoteId() {

}

func setNoteId() {
	return
}

func createNote() {
	resp, err := http.Get("things:///add?title=TaskThing&list=someday&tags=TaskThing")
	if err != nil {
		log.Fatal("No contact with things!")
	}
	fmt.Println(resp)
}

func getConfigPath() {
	return
}

func setConfigPath() {
	return
}

func createConfig() {
	return
}

func readPath(path string) simpleyaml.Yaml {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	return parseConfig(data)
}

func parseConfig(data []byte) simpleyaml.Yaml {
	c, err := simpleyaml.NewYaml(data)
	if err != nil {
		fmt.Printf("error %e", err)
		os.Exit(69)
	}

	if !checkRequired(*c) {
		fmt.Println("Failed to validate config!")
		os.Exit(69)
	}
	return *c
}

func checkRequired(config simpleyaml.Yaml) bool {
	// works for now...
	taskBin := config.Get("taskwarrior").Get("binaryPath")
	if taskBin.IsFound() {
		val, e := taskBin.String()
		if e != nil {
			println(e)
			os.Exit(69)
		}
		if !regexMatch(`(task)$`, val) {
			return false
		}
	} else {
		fmt.Printf("Couldn't find taskwarrior.binaryPath")
		return false
	}

	thingsApiKey := config.Get("things").Get("apiKey")
	if thingsApiKey.IsFound() {
		val, e := thingsApiKey.String()
		if e != nil {
			println(e)
			os.Exit(69)
		}
		if !regexMatch(`^([0-9A-Za-z]*)$`, val) {
			return false
		}
	} else {
		fmt.Printf("Couldn't find things.apiKey")
		return false
	}
	return true
}

func regexMatch(pattern string, subject string) bool {
	if len(subject) < 1 {
		fmt.Printf("Weird or no path set for %v. Got: %v", subject, subject)
		return false
	}
	res, _ := regexp.MatchString(pattern, subject)
	if !res {
		fmt.Printf("%v doesn't seem to be correct!? %v didnt match %v", subject, subject, pattern)
		return false
	}
	return true
}
