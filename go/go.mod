module mamoco/taskthing

go 1.16

require (
	github.com/alexflint/go-arg v1.4.2
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/smallfish/simpleyaml v0.1.0
)

replace configuration => ./configuration
