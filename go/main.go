package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/alexflint/go-arg"
	"github.com/smallfish/simpleyaml"
)

var (
	opString string

	args struct {
		I bool
	}
	config simpleyaml.Yaml
)

func init() {
	arg.MustParse(&args)
	if !args.I {
		showIntro()
	}

	opString = `
1) Move tasks from Taskwarrior to Things
2) Move tasks from Things to Taskwarrior

0) Enter interactive configuration
C) Display configuration
X) Exit

Type your choice and press enter: `
}

func main() {

	config = validateConfiguration()
	exportJsonFromTaskWarrior()

	os.Exit(0)

	fmt.Print(opString)

	reader := bufio.NewReader(os.Stdin)

	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("An error occured while reading input. Please try again", err)
		return
	}

	input = strings.TrimSuffix(input, "\n")

	switch input {
	case "1":
		fmt.Println("Move tasks from Taskwarrior to Things")
	case "2":
		fmt.Println("Move tasks from Things to TaskWarrior")
	case "0":
		fmt.Println("Configurator")
	case "C":
		fmt.Println("Your configuration:")
	case "X":
		os.Exit(0)
	}

	os.Exit(0)
}

func showIntro() {
	introAscii := [9]string{
		`=========================================================`,
		`    _________   _____ __ __________  _______   ________ `,
		`   /_  __/   | / ___// //_/_  __/ / / /  _/ | / / ____/ `,
		`    / / / /| | \__ \/ ,<   / / / /_/ // //  |/ / / __   `,
		`   / / / ___ |___/ / /| | / / / __  // // /|  / /_/ /   `,
		`  /_/ /_/  |_/____/_/ |_|/_/ /_/ /_/___/_/ |_/\____/    `,
		` `,
		`============ a pxpxpx(tm) devbug adventure ==============`,
		` `,
	}

	for x := 0; x < len(introAscii); x++ {
		v := (rand.Intn(50-10) + 100) * 1000000
		fmt.Println(introAscii[x])
		//time.Sleep((100) * time.Millisecond)
		time.Sleep(time.Duration(v))
	}
	time.Sleep(1500 * time.Millisecond)
}
