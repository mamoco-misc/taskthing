package main

import (
	"fmt"
	"log"
	"os/exec"
	"time"

	"github.com/buger/jsonparser"
)

func init() {

}

func exportJsonFromTaskWarrior() {
	task, args, _ := buildTaskExport()

	if res, err := exec.Command(task, args...).Output(); res == nil && err != nil {
		log.Fatal("Failed during export command")
	} else {
		//data := string(res)
		urls := buildThingsJson(res)
		fmt.Printf("Len: %v", len(urls))
	}

}

func buildTaskExport() (binary string, args []string, filename string) {

	workdir := "/Users/px/dev/TaskThing/_slask/" // => /tmp/

	binary, err := config.GetPath("taskwarrior", "binaryPath").String()
	if err != nil {
		fmt.Printf("Failed to get taskwarrior.binaryPath. Got '%v'", err)
	}

	filters, err := config.Get("taskwarrior").Get("exportFilter").Array()
	if err != nil {
		fmt.Printf("Failed to get taskwarrior.exportFilter. Got %v", err)
	} else {
		if len(filters) > 0 {
			for _, e := range filters {
				filter := fmt.Sprintf("%v", e)
				args = append(args, filter)
			}
		}
	}

	currentTime := time.Now().Local()
	timeString := fmt.Sprintf("%d%02d%02d%02d%02d%02d", currentTime.Year(), currentTime.Month(), currentTime.Day(), currentTime.Hour(), currentTime.Minute(), currentTime.Second())

	args = append(args, "export")
	filename = workdir + timeString + "taskwarrior.json"
	return binary, args, filename
}

func getAlreadyAddedItemsFromThings() string {
	// check if we have an ID stored
	// uuid, err := os.ReadFile()
	// resp, err := http.Get("http://example.com/")
	return "apa"
}

/*
	https://taskwarrior.org/docs/design/task.html
	NAME 			TYPE
	status 			String
	uuid 			UUID		32bit hex, alnum, 296d835e-8f85-4224-8f36-c612cad1b9f8
	parent 			UUID
	description 	String
	entry 			Date		ISO8601 20120110T231200Z
	start 			Date		-- " --
	end 			Date		-- " --
	due 			Date		-- " --
	until 			Date	    -- " --
	wait 			Date	    -- " --
	modified 		Date	    -- " --
	scheduled 		Date	    -- " --
	recur 			String
	mask 			String
	imask 			Integer
	project 		String
	priority 		String
	depends 		String
	tags * 			String
	annotation * 	String
	* Both tags and annotations are lists of strings and objects.
*/

func buildThingsJson(data []byte) []string {
	var Urls []string
	//var Projects []string
	jsonparser.ArrayEach(data, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		jsonparser.ObjectEach(value, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
			fmt.Printf("Key: '%s'\n Value: '%s'\n Type: %s\n", string(key), string(value), dataType)
			Urls = append(Urls, fmt.Sprintf("Key: '%s'\n Value: '%s'\n Type: %s\n", string(key), string(value), dataType))
			return nil
		})
	})

	return Urls
}
