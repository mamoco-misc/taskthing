import json
import re
from os import system
from datetime import datetime
from time import sleep
import urllib.parse

"""
Exports data from TaskWarrior and inserts it in Things for OS X

REQUIREMENTS:
1 ) Edit the config file taskthing.json that should be located in the same directory as the script.
2 ) Create a tag in Things called "from Taskwarrior" (or w/e name you enterd in the config-file)
3 ) Create an area in Things called "From Taskwarrior" (or w/e name you enterd in the config-file)
4 ) Make sure Things is up and running.
5 ) Run this file! $> python taskThing.py
"""
class ThingsTaskCreator(object):
    """
    Call on me!
    """
    def __init__(self):
        super(ThingsTaskCreator, self).__init__()
        #self.arg = arg
        self.projectsList = []
        self.areasList = []
        self.thingsTasks = []
        # dev
        # TODO: Make a method to read the config from file
        self.config = {
            'taskBinary': '/usr/local/bin/task',
            'jsonUrl': '/Users/px/taskwarrior.json',
            'filters': 'status:pending',
            'sleepTime': 1 # Very arbitrary. It's just to not make Things choke.
        }

        self.exportTaskWarrior()
        self.readJson(self.config['jsonUrl'])
        self.callThings(self.thingsTasks)

    """
    Exports the data
    @return void
    @notes: Things doesn't really like lots of data so set it to status:pending. You might want 
            to increase sleeptime, or add more filters, if Things does the beachball of death. YMMV.
    """
    def exportTaskWarrior(self):
        return
        system(f"{self.config['taskBinary']} {self.config['filters']} export > {self.config['jsonUrl']}")
        

    """
    Read JSON from TaskWarrior export and loop through it
    """
    def readJson(self, path):
        with open(path, 'r') as myfile:
            data = myfile.read()
            tasks = json.loads(data)
            if tasks:
                n=0 # debug 
                for x in tasks:
                    n = n+1
                    self.createThingsJson(x)
                    if n == 10:
                        break

    """
    Things doesn't like empty values... 
    """
    def createThingsJson(self,tWTask):
        notes = {}
        todo = {
            'type': 'to-do',
            'operation': "create",
            'attributes': {
                # 'title': "",
                # #'notes': {},    # implode later
                # 'when': "",     # today, tomorrow, evening, anytime, someday, a date string, or a date time string
                # 'deadline': "",
                # 'list': "",
                # 'creation-date': "",
                # 'completion-date': "",
                # 'completed': "",
                # 'canceled': "",
                'tags': ["From Taskwarrior"],
            }
        }
        if 'description' in tWTask:
            todo['attributes']['title'] = tWTask['description']
        if 'entry' in tWTask:
            todo['attributes']['creation-date'] = self.createThingsDate(tWTask['entry'])
        if 'due' in tWTask:
            todo['attributes']['deadline'] = self.createThingsDate(tWTask['due'])

        # here comes the fun stuff... 
        if 'project' in tWTask:
            result = self.createProject(tWTask['project'])
            if result:
                if len(result) > 1:
                    notes['Area'] = result[0]
                    notes['Project'] = result[1]
                    todo['attributes']['list'] = notes['Project']
                elif len(result) == 1:
                    notes['Area'] = result[0]
        if 'tags' in tWTask:
            notes['Tags'] = ", ".join(tWTask['tags'])
        if 'priority' in tWTask:
            notes['Priority'] = tWTask['priority']
        if 'urgency' in tWTask:
            notes['Urgency'] = tWTask['urgency']
        if 'uuid' in tWTask:
            notes['Uuid'] = tWTask['uuid']
        
        # smash the notes together
        todo['attributes']['notes'] = self.implodeNotes(notes)

        # add to the thingsTask-dict grouped by project (list)
        if 'Project' in notes:
            for i in range(len(self.projectsList)):
                p  = notes['Project']
                t  = self.projectsList[i]
                if t == p:
                    print(f"found {p} at index: {i}")
                    self.thingsTasks[i]['attributes']['items'].append(todo)
                
    """ 
    Implodes the notes to a long string 
    """
    def implodeNotes(self,dict):
        noteString = ""
        for k in dict:
            if not isinstance(dict[k], str):
                dict[k] = str(dict[k])
            noteString = noteString + k + ": " + dict[k] + "\n"
        return noteString

    """
    Taskwarriors dates are rendered in ISO8601 combined date and time in UTC format using the 
    template: YYYYMMDDTHHMMSSZ (20120110T231200Z)
    Things wants the dates in the format YYYY-MM-DDTHH:MM:SSZ+00:00 (2018-03-10T14:30:00Z+01:00)
    TZs are a pain and if that's a problem for you, please fork the repo!
    """
    def createThingsDate(self,taskWarriorISO): 
        taskTime = datetime.strptime(taskWarriorISO, '%Y%m%dT%H%M%SZ')
        thingsTime = taskTime.isoformat() + "+00:00"
        return thingsTime

    """ 
    Parses the project attribute from Taskwarrior. Splits by first space, dot, comma or minus 
    () to create the "area". Since areas cannot be created at the time with Things-URIs they'll be
    added as a note only.

    Ex. 'ParentName.ChildName.Task' becomes => Area: ParentName, Project: ChildName.Task
    """
    def createProject(self,string):
        # [{
        #   "type": "project",
        #   "attributes": {
        #     "title": "Go Shopping",
        #     "items": []
        #   }
        # }]
        print(f"Trying: {string}")
        if len(string):
            firstDelimiter = re.search('[ ._-]+', string)
            if firstDelimiter:
                pos = firstDelimiter.start()
                if (pos > 3):
                    area = string[0:firstDelimiter.start()]
                    if area not in self.areasList:
                        self.areasList.append(area)
                    project = string[firstDelimiter.end():len(string)]
                    self.addProjectToThingsTasks(project)
                    return [area,project]
            else:
                if string not in self.areasList:
                    self.areasList.append(string)
                    return [string]
    
    """
    Adds the project to the ThingsTask-list to be populated by todo's.
    """
    def addProjectToThingsTasks(self, projectName):
        project = {
            "type": "project",
            "attributes" : {
                "title": projectName,
                "area": "From Taskwarrior",
                "items": []
            }
        }
        # keep track of added projects
        if projectName not in self.projectsList:
            self.projectsList.append(projectName)
            # add to ThingsTasks
            self.thingsTasks.append(project)
        return True

    """ 
    Dumps the dict to JSON, encodes it as a list with the project as the only object
    and pushes it to Things. This in order to not cause Things to hickup.
    """
    def callThings(self, dict):
        for x in range(len(dict)):
            if self.sleepTime == None:
                sleepTime = len(dict[x]['attributes']['items']) / 2
            thingJson = json.dumps([dict[x]], indent=None, separators=(',', ':'), ensure_ascii=False)
            encJson = urllib.parse.quote(thingJson, safe='/:,', encoding="UTF-8")
            thingsUrl = "things:///json?data=" + encJson
            
            system(f"open {thingsUrl}")
            sleep(self.sleepTime)

new = ThingsTaskCreator()